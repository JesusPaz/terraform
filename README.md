# Terraform repository

This terraform repository is composed of:
* **Modules**: The implementation of a terraform resource that can be modified based on certain variables. They are generic in order to seek code reuse.
* **Deployments**: The instance of one or several modules to generate an architecture composed of multiple resources.
