variable "aws_region" {
  type        = string
  description = "AWS region"
  default     = "us-east-1"
}

variable "allocated_storage" {
  type        = number
  description = "Allocated storage"
  default     = 10
}

variable "engine" {
  type        = string
  description = "Engine"
  default     = "mysql"
}

variable "engine_version" {
  type        = string
  description = "Engine version"
  default     = "5.7"
}

variable "instance_class" {
  type        = string
  description = "Instance class"
  default     = "db.t2.micro"
}

variable "parameter_group_name" {
  type        = string
  description = "Parameter group name"
  default     = "default.mysql5.7"
}

variable "skip_final_snapshot" {
  type        = bool
  description = "skip final snapshot"
  default     = true
}

variable "db_name" {
  type        = string
  description = "Database name"
  default     = "accountapi"
}

variable "db_identifier" {
  type        = string
  description = "Database identifier"
}

variable "db_user" {
  type        = string
  description = "Database user"
}

variable "db_password" {
  type        = string
  description = "Database password"
}


variable "publicly_accessible" {
  type        = bool
  description = "Publicly accessible"
  default     = true
}
