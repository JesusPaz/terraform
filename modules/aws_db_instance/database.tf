resource "aws_db_instance" "default" {
  identifier             = var.db_identifier
  name                   = var.db_name
  username               = var.db_user
  password               = var.db_password
  allocated_storage      = var.allocated_storage
  engine                 = var.engine
  engine_version         = var.engine_version
  instance_class         = var.instance_class
  parameter_group_name   = var.parameter_group_name
  skip_final_snapshot    = var.skip_final_snapshot
  publicly_accessible    = var.publicly_accessible
  vpc_security_group_ids = [aws_security_group.db_sg.id]
}
