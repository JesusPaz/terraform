variable "cluster_name" {
  type        = string
  description = "ECS cluster name"
}

variable "aws_region" {
  type        = string
  description = "AWS region"
  default     = "us-east-1"
}
