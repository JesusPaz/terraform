########################
# Deployment Information
########################
# name: aws-ecs-app
# version: 0.1.2
# description: AWS ECS cluster
### Log
# 0.1.0 [Feature] Base deployment
# 0.1.1 [Feature] Enable ECS container logs
# 0.1.2 [Feature] Add auto scaling
