resource "aws_ecs_task_definition" "task_definition" {
  family                   = var.task_definition_family
  container_definitions    = <<DEFINITION
  [
    {
      "name": "${var.task_definition_family}",
      "image": "${var.docker_image}",
      "memory": ${var.memory},
      "cpu": ${var.cpu},
      "essential": true,
      "environment": ${jsonencode(var.task_environment)},
      "portMappings": [
        {
          "containerPort": ${var.container_port},
          "hostPort": ${var.host_port}
        }
      ],
      "logConfiguration": {
        "logDriver": "awslogs",
        "options": {
            "awslogs-group": "${var.logs_group}",
            "awslogs-region": "${var.logs_region}",
            "awslogs-stream-prefix": "${var.logs_stream_prefix}"
        }
      }
    }
  ]
  DEFINITION
  requires_compatibilities = ["FARGATE"]
  network_mode             = "awsvpc"
  memory                   = var.memory
  cpu                      = var.cpu
  execution_role_arn       = data.aws_iam_role.ecsTaskExecutionRole.arn
}

resource "aws_ecs_service" "service" {
  name            = var.service_name
  cluster         = data.aws_ecs_cluster.ecs_cluster.id
  task_definition = aws_ecs_task_definition.task_definition.arn
  launch_type     = "FARGATE"
  desired_count   = var.desired_count

  load_balancer {
    target_group_arn = data.aws_lb_target_group.target_group.arn
    container_name   = aws_ecs_task_definition.task_definition.family
    container_port   = var.container_port
  }

  network_configuration {
    subnets          = [aws_default_subnet.default_subnet_a.id, aws_default_subnet.default_subnet_b.id, aws_default_subnet.default_subnet_c.id]
    assign_public_ip = true
    security_groups  = [aws_security_group.service_security_group.id]
  }
}
