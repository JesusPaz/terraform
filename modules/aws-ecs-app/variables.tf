variable "cluster_name" {
  type        = string
  description = "ECS cluster name"
}

variable "docker_image" {
  type        = string
  description = "docker image"
}

variable "task_definition_family" {
  type        = string
  description = "task definition family"
}

variable "service_name" {
  type        = string
  description = "service name"
}

variable "lb_sg_name" {
  type        = string
  description = "lb sg name"
}

variable "lb_tg_name" {
  type        = string
  description = "lb tg name"
}

variable "aws_region" {
  type        = string
  description = "AWS region"
  default     = "us-east-1"
}

variable "container_port" {
  type        = string
  description = "container port"
  default     = "80"
}

variable "host_port" {
  type        = string
  description = "host port"
  default     = "80"
}

variable "memory" {
  type        = string
  description = "Container memory"
  default     = "512"
}

variable "cpu" {
  type        = string
  description = "Container cpu"
  default     = "256"
}

variable "desired_count" {
  type        = string
  description = "desired count"
  default     = "1"
}

variable "task_environment" {
  default = []
}

variable "logs_group" {
  type        = string
  description = "Logs group"
}

variable "logs_region" {
  type        = string
  description = "Logs region"
}

variable "logs_create_group" {
  type        = string
  description = "Logs create group"
  default     = "true"
}

variable "logs_stream_prefix" {
  type        = string
  description = "Logs stream prefix"
}

variable "max_capacity" {
  type        = number
  description = "Auto scaling max capacity"
  default     = 5
}

variable "min_capacity" {
  type        = string
  description = "Auto scaling min capacity"
  default     = 1
}

variable "memory_policy_target_value" {
  type        = string
  description = "Memory policy target value"
  default     = 80
}

variable "cpu_policy_target_value" {
  type        = string
  description = "CPU policy target value"
  default     = 80
}
