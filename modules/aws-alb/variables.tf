variable "alb_name" {
  type        = string
  description = "ALB name"
}

variable "aws_region" {
  type        = string
  description = "AWS region"
  default     = "us-east-1"
}

variable "target_group_name" {
  type        = string
  description = "Target group name"
}

variable "security_group_name" {
  type        = string
  description = "Security group name"
}

variable "domain" {
  type        = string
  description = "Domain"
}
