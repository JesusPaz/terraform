variable "environment" {
  type        = string
  description = "Environment"
}

variable "aws_region" {
  type        = string
  description = "AWS region"
  default     = "us-east-1"
}

variable "db_user" {
  type        = string
  description = "Database user"
  default     = ""
}

variable "vault_path" {
  type        = string
  description = "Vault path"
  default     = ""
}
