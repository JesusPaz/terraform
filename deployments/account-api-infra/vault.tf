locals {
  vault_path = coalesce(var.vault_path, "secret-${var.environment}/account-api")
}

resource "vault_generic_secret" "database_credentials" {
  path = local.vault_path

  data_json = <<EOT
{
  "user": "${local.db_user}",
  "password": "${random_password.password.result}"
}
EOT
}
