locals {
  db_user = coalesce(var.db_user, "account_api")
}

resource "random_password" "password" {
  length           = 16
  special          = true
  override_special = "_%@"
}

module "db" {
  source        = "s3::https://s3.amazonaws.com/terraform-resources-jp/aws-db-instance-0.1.1.zip"
  db_identifier = "${var.environment}-account-api-db"
  db_user       = local.db_user
  db_password   = random_password.password.result
}
