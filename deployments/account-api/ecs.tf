module "ecs-app-deployment" {
  source                 = "s3::https://s3.amazonaws.com/terraform-resources-jp/aws-ecs-app-0.1.2.zip"
  cluster_name           = "${var.environment}-ecs-cluster"
  docker_image           = "registry.gitlab.com/jesuspaz/account-api:${var.image_version}"
  task_definition_family = "${var.environment}-account-api-task"
  service_name           = "${var.environment}-account-api-svc"
  lb_sg_name             = "${var.environment}-ecs-alb-sg"
  lb_tg_name             = "${var.environment}-ecs-alb-tg"
  task_environment       = local.task_environment
  logs_group             = "${var.environment}-account-api"
  logs_region            = "us-east-1"
  logs_stream_prefix     = "ecs-${var.environment}-acc"
}
