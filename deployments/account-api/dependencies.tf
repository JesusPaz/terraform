data "vault_generic_secret" "database_credentials" {
  path = local.vault_path
}

data "aws_db_instance" "database" {
  db_instance_identifier = "${var.environment}-account-api-db"
}

locals {
  vault_path   = coalesce(var.vault_path, "secret-${var.environment}/account-api")
  db_user      = data.vault_generic_secret.database_credentials.data["user"]
  db_pass      = data.vault_generic_secret.database_credentials.data["password"]
  app_settings = var.environment == "prod" ? "config.ProdConfig" : "config.DevConfig"
  database_uri = "mysql+pymysql://${local.db_user}:${local.db_pass}@${data.aws_db_instance.database.endpoint}/${var.default_database}"
  task_environment = [
    {
      "name"  = "APP_SETTINGS",
      "value" = local.app_settings
    },
    {
      "name"  = "DATABASE_URI",
      "value" = local.database_uri
  }]
}
