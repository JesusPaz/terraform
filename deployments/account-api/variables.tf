variable "environment" {
  type        = string
  description = "Environment"
}

variable "aws_region" {
  type        = string
  description = "AWS region"
  default     = "us-east-1"
}

variable "image_version" {
  type        = string
  description = "image version"
  default     = "develop"
}

variable "default_database" {
  type        = string
  description = "default database"
  default     = "accountapi"
}

variable "vault_path" {
  type        = string
  description = "Vault path"
  default     = ""
}
