module "alb" {
  source              = "s3::https://s3.amazonaws.com/terraform-resources-jp/aws-alb-0.1.1.zip"
  alb_name            = "${var.environment}-ecs-alb"
  target_group_name   = "${var.environment}-ecs-alb-tg"
  security_group_name = "${var.environment}-ecs-alb-sg"
  domain              = "*.jesuspaz.com"
}
