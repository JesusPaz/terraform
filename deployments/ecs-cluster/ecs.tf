module "ecs-cluster" {
  source       = "s3::https://s3.amazonaws.com/terraform-resources-jp/aws-ecs-cluster-0.1.1.zip"
  cluster_name = "${var.environment}-ecs-cluster"
}
